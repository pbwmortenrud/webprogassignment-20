'use strict';

const $ = function (foo) {
    return document.getElementById(foo);
};
const $c = function (foo) {
    return document.getElementsByClassName(foo);
};

const setTitles = function (title) {
    let targets = document.getElementsByClassName('title');
    for(let i = 0; i < targets.length; ++i) {
        targets[i].innerHTML = title;
    }
};



const setFooter = function (bar, year) {
    let now = new Date();
    $('foot').innerHTML = `&copy; ${bar} ${year}-${now.getFullYear()}`;
}


const doSomething = function () {
    setTitles('Assignment Web Programming.2.0');
    setFooter('Rud', 2021);
}
window.addEventListener('load', doSomething);