let isDebug = true;

const fact = function (n) {
  if (n <= 1) return 1;
  else return n * fact(n - 1);
};

const fibo = function (n) {
  if (n <= 2) return 1;
  else return fibo(n - 1) + fibo(n - 2);
};

const pow = function (r, e) {
  if (e === 0) return 1;
  else return r * pow(r, e - 1);
};

//Kan nok gøres kortere!!
const isPalindrome = function (s) {
  s = s.toLowerCase().split(" ").join("");
  if (isDebug) {
    console.log(s);
  }
  if (s.length <= 0) return console.error("string contains nothing");
  if ((s.length <= 2 && s[0] === s[1]) || s.length == 1) return true;
  if (s[0] == s[s.length - 1] && s.length > 2) {
    s = s.slice(1, s.length - 1);
    return isPalindrome(s);
  } else return false;
};
